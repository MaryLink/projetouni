create database ProjetoUni;
use ProjetoUni;

create table Tb_Fornecedores(
id_fornecedor int primary key auto_increment
,nm_fornecedor varchar(50) NOT null
,ds_telefone varchar(11) not null
,ds_cnpj varchar(15)
,ds_email varchar(50)
,ds_endereco varchar(40)
,ds_bairro varchar(40)
,nr_residencial varchar(5)
,de_cep varchar(11)
,ds_categoria varchar(30)
);

create table Tb_Clientes(
nm_cliente varchar(50) not null
,ds_email varchar(50)
,ds_fidelidade varchar(15)
,ds_telefone varchar(15)
,ds_celular varchar(15)
,ds_cpf varchar(15)
,ds_cep varchar(15)
,ds_estado varchar(40)
);

create table Tb_Departamentos(
id_departamento int primary key auto_increment
,nm_departamento varchar(40)
,vl_salario decimal
);

create table Tb_Funcionarios(
id_funcionario int primary key auto_increment
,nm_funcionario varchar(50)
,ds_email varchar(50)
,ds_telefone varchar(11)
,ds_celular varchar (11)
,dt_nascimento datetime
,dt_contrato datetime
,ds_cep varchar(8)
,ds_rg varchar(10)
,ds_cpf varchar(11)
,hr_entrada datetime
,hr_saida datetime
,imagem mediumblob
,fk_departamento int
 ,constraint fk_departamento foreign key (fk_departamento) references Tb_Departamentos (id_departamento)
);

create table Tb_Usuarios(
id_usuario int primary key auto_increment
,ds_nickname varchar(50)
,ds_senha varchar(50)
,fk_funcionario int
,constraint fk_funcionario foreign key (fk_funcionario) references Tb_funcionarios (id_funcionario)
);

create table Tb_PontoTrabalhador(
id_PontoTrabalhador int primary key auto_increment
,nm_empresa varchar(50)
,ds_cnpj varchar(14)
,endereco_empresa varchar(50)
,ds_nf varchar(14)
,ds_pis varchar(12)
,ds_nsr varchar(10)
,hr_entrada datetime
,hr_saida datetime
,vl_ganho decimal
,fk_funcionario int, foreign key (fk_funcionario) references Tb_funcionarios (id_funcionario) 
);

create table Tb_Salario(
id_salario int primary key auto_increment,
fk_funcionario int, foreign key (fk_funcionario) references Tb_funcionarios (id_funcionario),
fk_departamento int,  foreign key (fk_departamento) references Tb_departamentos (id_departamento)
);