﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoUni.Telas.Cadastrar;

namespace ProjetoUni.Telas
{
    public partial class FrmInicial : Form
    {
        public FrmInicial()
        {
            InitializeComponent();
        }

        public void CarregarTela(UserControl tela)
        {
            panelTelaInicial.Controls.Clear();

            if (panelTelaInicial.Controls.Count > 3)
            {
                panelTelaInicial.Controls.RemoveAt(panelTelaInicial.Controls.Count -1);
            }

            panelTelaInicial.Controls.Add(tela);

        }

        private void DToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDepartamentoCadastro tela = new FrmDepartamentoCadastro();
            CarregarTela(tela);
        }

        private void FuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFuncionarioCadastro tela = new FrmFuncionarioCadastro();
            CarregarTela(tela);
        }

        private void ClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmClienteCadastro tela = new FrmClienteCadastro();
            CarregarTela(tela);
        }
    }
}
