﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoUni.Classes.Funcionario;
using ProjetoUni.Classes.Departamento;

namespace ProjetoUni.Telas.Cadastrar

{
    public partial class FrmFuncionarioCadastro : UserControl
    {
        public FrmFuncionarioCadastro()
        {
            InitializeComponent();

            DepartamentoBusiness bus = new DepartamentoBusiness();
            List<DepartamentoDTO> list = new List<DepartamentoDTO>();
            list = bus.Listar();

            List<string> listString = new List<string>();
            foreach (DepartamentoDTO item in list)
            {
                listString.Add(item.Nome);
            }
            cboDepartamento.DataSource = listString;
        }

        private void BtnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = txtNome.Text;
                dto.RG = txtRG.Text;
                dto.Dt_Contrato = dtContrato.Value;
                dto.Dt_Nascimento = dtNasc.Value;
                dto.Email = txtEmail.Text;
                dto.CPF = txtCPF.Text;
                dto.Telefone = txtTelefone.Text;
                dto.CEP = txtCep.Text;
                dto.Celular = txtCelular.Text;

                FuncionarioBusiness bus = new FuncionarioBusiness();
                bus.Salvar(dto);

                MessageBox.Show("Salvo com Sucesso");



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
