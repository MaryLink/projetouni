﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoUni.Classes.Departamento;

namespace ProjetoUni.Telas.Cadastrar
{
    public partial class FrmDepartamentoCadastro : UserControl
    {
        public FrmDepartamentoCadastro()
        {
            InitializeComponent();
        }

        private void BtnSalvarDepartamento_Click(object sender, EventArgs e)
        {
            try
            {
                DepartamentoDTO dto = new DepartamentoDTO();
                dto.Nome = txtNome.Text;
                dto.Salario = nudSalario.Value;
                dto.Beneficios = txtBeneficios.Text;

                DepartamentoBusiness bus = new DepartamentoBusiness();
                bus.Salvar(dto);

                MessageBox.Show("Salvo com sucesso");
            }
            catch (Exception er)
            {

                MessageBox.Show(er.Message);
            }
           
        }
    }
}
