﻿namespace ProjetoUni.Telas.Cadastrar
{
    partial class FrmDepartamentoCadastro
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.txtBeneficios = new System.Windows.Forms.TextBox();
            this.btnSalvarDepartamento = new System.Windows.Forms.Button();
            this.txtNome = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(249, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(339, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cadastrar Departamento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(69, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 31);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nome:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(59, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 31);
            this.label3.TabIndex = 3;
            this.label3.Text = "Salário:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(17, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 31);
            this.label4.TabIndex = 4;
            this.label4.Text = "Benefícios:";
            // 
            // nudSalario
            // 
            this.nudSalario.Font = new System.Drawing.Font("Arial", 12F);
            this.nudSalario.Location = new System.Drawing.Point(171, 131);
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(210, 26);
            this.nudSalario.TabIndex = 7;
            // 
            // txtBeneficios
            // 
            this.txtBeneficios.Font = new System.Drawing.Font("Arial", 12F);
            this.txtBeneficios.Location = new System.Drawing.Point(171, 172);
            this.txtBeneficios.Name = "txtBeneficios";
            this.txtBeneficios.Size = new System.Drawing.Size(210, 26);
            this.txtBeneficios.TabIndex = 8;
            // 
            // btnSalvarDepartamento
            // 
            this.btnSalvarDepartamento.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSalvarDepartamento.Font = new System.Drawing.Font("Arial", 12F);
            this.btnSalvarDepartamento.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSalvarDepartamento.Location = new System.Drawing.Point(171, 222);
            this.btnSalvarDepartamento.Name = "btnSalvarDepartamento";
            this.btnSalvarDepartamento.Size = new System.Drawing.Size(210, 40);
            this.btnSalvarDepartamento.TabIndex = 9;
            this.btnSalvarDepartamento.Text = "Salvar";
            this.btnSalvarDepartamento.UseVisualStyleBackColor = false;
            this.btnSalvarDepartamento.Click += new System.EventHandler(this.BtnSalvarDepartamento_Click);
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(169, 93);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(212, 20);
            this.txtNome.TabIndex = 10;
            // 
            // FrmDepartamentoCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.btnSalvarDepartamento);
            this.Controls.Add(this.txtBeneficios);
            this.Controls.Add(this.nudSalario);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmDepartamentoCadastro";
            this.Size = new System.Drawing.Size(859, 478);
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.TextBox txtBeneficios;
        private System.Windows.Forms.Button btnSalvarDepartamento;
        private System.Windows.Forms.TextBox txtNome;
    }
}
