﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoUni.Classes.Cliente;

namespace ProjetoUni.Telas.Cadastrar
{
    public partial class FrmClienteCadastro : UserControl
    {
        public FrmClienteCadastro()
        {
            InitializeComponent();
        }

        private void BtnSalvarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                ClienteBusiness bus = new ClienteBusiness();

                dto.Nome = txtNome.Text;
                dto.Celular = txtCelular.Text;
                dto.CEP = txtCep.Text;
                dto.CPF = txtCPF.Text;
                dto.Email = txtEmail.Text;
                dto.Estado = txtEmail.Text;
                dto.Fidelidade = cboFidelidade.Text;
                dto.Telefone = txtTelefone.Text;

                bus.Salvar(dto);

                MessageBox.Show("Salvo com Sucesso");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            


        }
    }
}
