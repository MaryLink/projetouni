﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Base
{
    public class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "Server=localhost;Database=ProjetoUni;Uid =root;password=1234";

            MySqlConnection Connection = new MySqlConnection(connectionString);

            Connection.Open();

            return Connection;
        }
    }
}
