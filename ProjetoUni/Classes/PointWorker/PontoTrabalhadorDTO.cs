﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.PointWorker
{
  public class PontoTrabalhadorDTO
    {
        public int ID { get; set; }
        public string Nm_Empresa { get; set; }
        public string  CNPJ { get; set; }
        public string Endereco_Empresa { get; set; }
        public string NF { get; set; }
        public string PIS { get; set; }
        public string NSR { get; set; }
        public DateTime Entrada { get; set; }
        public DateTime Saida { get; set; }
        public decimal ganho { get; set; }
        public int Fk_Funcionario { get; set; }

    }
}
