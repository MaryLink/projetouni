﻿using MySql.Data.MySqlClient;
using ProjetoUni.Classes.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Cliente
{
  public class ClienteDatabase
    {
        DATABASE db = new DATABASE();
        public int Salvar(ClienteDTO cliente) 
        {
            string script = @"INSERT INTO Tb_Clientes(nm_cliente, ds_email, ds_fidelidade, ds_telefone, ds_celular, ds_cpf, ds_cep, ds_estado)
                             VALUES (@nm_cliente    
                                     ,@ds_email
                                     ,@ds_fidelidade
                                     ,@ds_telefone
                                     ,@ds_celular
                                     ,@ds_cpf
                                     ,@ds_cep
                                     ,@ds_estado)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente.Nome));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_fidelidade", cliente.Fidelidade));
            parms.Add(new MySqlParameter("ds_telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_celular", cliente.Celular));
            parms.Add(new MySqlParameter("ds_cpf", cliente.CPF));
            parms.Add(new MySqlParameter("ds_cep", cliente.CEP));
            parms.Add(new MySqlParameter("ds_estado", cliente.Estado));

            int pk = db.ExecuteInsertScriptWithPK(script, parms);
            return pk; 
        }

        public void Alterar(ClienteDTO cliente)
        {
            string script = @"UPDATE Tb_Clientes SET
                                            ,nm_cliente     = @nm_cliente
                                            ,ds_email       = @ds_email
                                            ,ds_fidelidade  = @ds_fidelidade
                                            ,ds_telefone    = @ds_telefon
                                            ,ds_celular     = @ds_celular
                                            ,ds_cpf         = @ds_cpf
                                            ,ds_cep         = @ds_cep
                                            ,ds_estado      = @ds_estado
                                         ,WHERE id_cliente  = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", cliente.ID));
            parms.Add(new MySqlParameter("nm_cliente", cliente.Nome));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_fidelidade", cliente.Fidelidade));
            parms.Add(new MySqlParameter("ds_telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_celular", cliente.Celular));
            parms.Add(new MySqlParameter("ds_cpf", cliente.CPF));
            parms.Add(new MySqlParameter("ds_cep", cliente.CEP));
            parms.Add(new MySqlParameter("ds_estado", cliente.Estado));

            db.ExecuteInsertScript(script, parms);

        }

        public void Remover (int idCliente)
        {
            string script = @"DELETE FROM Tb_Clientes
                               WHERE id_Cliente = @id_Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", idCliente));

            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteDTO> Listar ()
        {
            string script = @"SELECT * FROM Tb_Clientes";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ClienteDTO> listaDTO = new List<ClienteDTO>();

            while(reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.ID = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Email = reader.GetString("ds_email");
                dto.Fidelidade = reader.GetString("ds_fidelidade");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Celular = reader.GetString("ds_celular");
                dto.CPF = reader.GetString("ds_cpf");
                dto.CEP = reader.GetString("ds_cep");
                dto.Estado = reader.GetString("ds_estado");

                listaDTO.Add(dto);
            }

            reader.Close();
            return listaDTO;
        }

        public List<ClienteDTO> Consultar (string nome)
        {
            string script = @"SELECT * FROM Tb_Clientes
                              WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> listaDTO = new List<ClienteDTO>();

            while(reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.ID = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Email = reader.GetString("ds_email");
                dto.Fidelidade = reader.GetString("ds_fidelidade");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Celular = reader.GetString("ds_celular");
                dto.CPF = reader.GetString("ds_cpf");
                dto.CEP = reader.GetString("ds_cep");
                dto.Estado = reader.GetString("ds_estado");

                listaDTO.Add(dto);

            }

            reader.Close();
            return listaDTO;

        }

       
    }
}
