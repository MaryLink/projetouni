﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Salario
{
 public class SalarioDTO
    {
        public int ID { get; set; }
        public int Fk_Funcionario { get; set; }
        public int Fk_Departamento { get; set; }

    }
}
