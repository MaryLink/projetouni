﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoUni.Classes.Funcionario;
using ProjetoUni.Classes.Base;
using MySql.Data.MySqlClient;
using ProjetoUni.Classes.Departamento;

namespace ProjetoUni.Classes.Funcionario
{
  public class FuncionarioDatabase
    {
        DATABASE db = new DATABASE();

        public int Salvar(FuncionarioDTO funcionario)
        {
            string script = @"INSERT INTO Tb_funcionarios (nm_funcionario, ds_email, ds_telefone, ds_celular, dt_nascimento, dt_contrato, ds_cep, ds_rg, ds_cpf, fk_departamento)
                              VALUES (@nm_funcionario,
                                      @ds_email,
                                      @ds_telefone,
                                      @ds_celular,
                                      @dt_nascimento,
                                      @dt_contrato,
                                      @ds_cep, 
                                      @ds_rg, 
                                      @ds_cpf, 
                                      @fk_departamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.Nome));
            parms.Add(new MySqlParameter("ds_email", funcionario.Email));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.Telefone));
            parms.Add(new MySqlParameter("ds_celular", funcionario.Celular));
            parms.Add(new MySqlParameter("dt_nascimento", funcionario.Dt_Nascimento));
            parms.Add(new MySqlParameter("dt_contrato", funcionario.Dt_Contrato));
            parms.Add(new MySqlParameter("ds_cep", funcionario.CEP));
            parms.Add(new MySqlParameter("ds_rg", funcionario.RG));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.CPF));
            parms.Add(new MySqlParameter("fk_departamento",funcionario.Fk_Departamento));

            int pk = db.ExecuteInsertScriptWithPK(script, parms);

            return pk;
        }

        public void Alterar (FuncionarioDTO funcionario)
        {
            string script = @"UPDATE Tb_funcionarios
                              SET nm_funcionario    = @nm_funcionario,
                                  ds_email          = @ds_email,
                                  ds_telefone       = @ds_telefone,
                                  ds_celular        = @ds_celular,
                                  dt_nascimemnto    = @dt_nascimento,
                                  dt_contrato       = @dt_contrato,
                                  ds_cep            = @ds_cep,
                                  ds_rg             = @ds_rg,
                                  ds_cpf            = @ds_cpf,
                               WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", funcionario.ID));
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.Nome));
            parms.Add(new MySqlParameter("ds_email", funcionario.Email));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.Telefone));
            parms.Add(new MySqlParameter("ds_celular", funcionario.Celular));
            parms.Add(new MySqlParameter("dt_nascimento", funcionario.Dt_Nascimento));
            parms.Add(new MySqlParameter("dt_contrato", funcionario.Dt_Contrato));
            parms.Add(new MySqlParameter("ds_cep", funcionario.CEP));
            parms.Add(new MySqlParameter("ds_rg", funcionario.RG));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.CPF));

            db.ExecuteInsertScript(script, parms);
        }
    }
}
