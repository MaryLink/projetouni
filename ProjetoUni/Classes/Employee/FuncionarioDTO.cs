﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Funcionario
{
   public class FuncionarioDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public DateTime Dt_Nascimento { get; set; }
        public DateTime Dt_Contrato { get; set; }
        public string CEP { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public DateTime Hr_Entrada { get; set; }
        public DateTime Hr_Saida { get; set; }
        public string Imagem { get; set; }
        public int Fk_Departamento { get; set; }
    }
}
