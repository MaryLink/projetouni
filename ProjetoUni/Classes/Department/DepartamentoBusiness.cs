﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Departamento
{
   public class DepartamentoBusiness
    {
        DepartamentoDatabase db = new DepartamentoDatabase();

        public int Salvar (DepartamentoDTO dto)
        {
            int pk = db.Salvar(dto);
            return pk;
        }

        public List<DepartamentoDTO> Listar()
        {
            return db.Listar();
            
        }
    }
}
