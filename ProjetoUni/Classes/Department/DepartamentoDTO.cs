﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Departamento
{
   public class DepartamentoDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public decimal Salario { get; set; }
        public string Beneficios { get; set; }
    }
}
