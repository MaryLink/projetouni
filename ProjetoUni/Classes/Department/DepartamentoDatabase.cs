﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using ProjetoUni.Classes.Base;
using ProjetoUni.Classes.Departamento;

namespace ProjetoUni.Classes.Departamento
{
   public class DepartamentoDatabase
    {
        DATABASE db = new DATABASE();

        public int Salvar (DepartamentoDTO departamento)
        {
            string script = @"INSERT INTO Tb_Departamentos (nm_departamento, vl_salario, ds_beneficios)
                              VALUES (@nm_departamento
                                     ,@vl_salario
                                     ,@ds_beneficios)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", departamento.Nome));
            parms.Add(new MySqlParameter("vl_salario", departamento.Salario));
            parms.Add(new MySqlParameter("ds_beneficios", departamento.Beneficios));

            int pk = db.ExecuteInsertScriptWithPK(script, parms);
            return pk;
        }

        public void Alterar (DepartamentoDTO departamento)
        {
            string script = @"UPDATE Tb_departamentos SET
                                                      nm_departamento     = @nm_departamento,
                                                      vl_salario          = @vl_salario,
                                                      ds_beneficios       = @ds_beneficios
                                                WHERE id_departamento     = @id_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_departamento", departamento.ID));
            parms.Add(new MySqlParameter("nm_departamento", departamento.Nome));
            parms.Add(new MySqlParameter("vl_salario", departamento.Salario));
            parms.Add(new MySqlParameter("ds_beneficios", departamento.Beneficios));

            db.ExecuteInsertScript(script, parms);
        }

        public void Remover (int idDepartamento)
        {
            string script = @"DELETE * FROM Tb_Departamentos
                                       WHERE id_departamento = @id_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_departamento", idDepartamento));

            db.ExecuteInsertScript(script, parms);
        }

        public List<DepartamentoDTO> Listar ()
        {
            string script = @"SELECT * FROM Tb_Departamentos";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DepartamentoDTO> listaDTO = new List<DepartamentoDTO>();

            while(reader.Read())
            {
                DepartamentoDTO dto = new DepartamentoDTO();
                dto.ID = reader.GetInt32("id_departamento");
                dto.Nome = reader.GetString("nm_departamento");
                dto.Salario = reader.GetDecimal("vl_salario");
                dto.Beneficios = reader.GetString("ds_beneficios");

                listaDTO.Add(dto);
            }

            reader.Close();
            return listaDTO;

        }

        public List<DepartamentoDTO> Consultar (string nome)
        {
            string script = @"SELECT * FROM Tb_Departamentos
                              WHERE nm_departamento = @nm_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", "%" + nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DepartamentoDTO> listaDTO = new List<DepartamentoDTO>();

            while(reader.Read())
            {
                DepartamentoDTO dto = new DepartamentoDTO();
                dto.ID = reader.GetInt32("id_departamento");
                dto.Nome = reader.GetString("nm_departamento");
                dto.Salario = reader.GetDecimal("vl_salario");
                dto.Beneficios = reader.GetString("ds_beneficios");

                listaDTO.Add(dto);
            }

            reader.Close();
            return listaDTO;
        }
    }
}
