﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Usuario
{
    class UsuarioDTO
    {
        public int ID { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Nome_Usuario { get; set; }
        public int Fk_Funcionario { get; set; }
    }
}
