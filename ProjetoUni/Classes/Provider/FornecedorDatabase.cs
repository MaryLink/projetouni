﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using ProjetoUni.Classes.Fornecedor;
using ProjetoUni.Classes.Base;

namespace ProjetoUni.Classes.Fornecedor
{
    public class FornecedorDatabase
    {
        DATABASE db = new DATABASE();

        public int Salvar(FornecedorDTO fornecedor)
        {
            string script = @"INSERT INTO Tb_Fornecedores (nm_fornecedor, ds_telefone, ds_CNPJ, ds_email, ds_endereco, ds_bairro, nr_residencial, ds_cep, ds_categoria)
                               VALUES (@nm_fornecedor,
                                       @ds_telefone,
                                       @ds_CNPJ,
                                       @ds_email,
                                       @ds_endereco,
                                       @ds_bairro,
                                       @nr_residencial,
                                       @ds_cep,
                                       @ds_categoria)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", fornecedor.Nome));
            parms.Add(new MySqlParameter("ds_telefone", fornecedor.Telefone));
            parms.Add(new MySqlParameter("ds_CNPJ", fornecedor.CNPJ));
            parms.Add(new MySqlParameter("ds_email", fornecedor.Email));
            parms.Add(new MySqlParameter("ds_endereco", fornecedor.Endereco));
            parms.Add(new MySqlParameter("ds_bairro", fornecedor.Bairro));
            parms.Add(new MySqlParameter("nr_residencial", fornecedor.Nr_Residencial));
            parms.Add(new MySqlParameter("ds_cep", fornecedor.CEP));
            parms.Add(new MySqlParameter("ds_categoria", fornecedor.Categoria));

            int pk = db.ExecuteInsertScriptWithPK(script, parms);
            return pk;
        }

        public void Alterar (FornecedorDTO fornecedor)
        {
            string script = @"UPDATE Tb_Fornecedores SET
                                                     nm_fornecedor      = @nm_fornecedor,
                                                     ds_telefone        = @ds_telefone,
                                                     ds_CNPJ            = @ds_CNPJ,
                                                     ds_email           = @ds_email,
                                                     ds_endereco        = @ds_endereco,
                                                     ds_bairro          = @ds_bairro,
                                                     nr_residencial     = @nr_residencial,
                                                     ds_cep             = @ds_cep,
                                                     ds_categoria       = @ds_categoria
                                                  WHERE id_fornecedor   = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", fornecedor.Nome));
            parms.Add(new MySqlParameter("ds_telefone", fornecedor.Telefone));
            parms.Add(new MySqlParameter("ds_CNPJ", fornecedor.CNPJ));
            parms.Add(new MySqlParameter("ds_email", fornecedor.Email));
            parms.Add(new MySqlParameter("ds_endereco", fornecedor.Endereco));
            parms.Add(new MySqlParameter("ds_bairro", fornecedor.Bairro));
            parms.Add(new MySqlParameter("nr_residencial", fornecedor.Nr_Residencial));
            parms.Add(new MySqlParameter("ds_cep", fornecedor.CEP));
            parms.Add(new MySqlParameter("ds_categoria", fornecedor.Categoria));

            db.ExecuteInsertScript(script, parms);

        }

        public void Remover (int idFornecedor)
        {
            string script = @"DELETE FROM Tb_Fornecedores
                              WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", idFornecedor));

            db.ExecuteInsertScript(script, parms);
        }

        public List<FornecedorDTO> Listar ()
        {
            string script = @"SELECT * FROM Tb_Fornecedores";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<FornecedorDTO> listaDTO = new List<FornecedorDTO>();

            while(reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.ID = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_fornecedor");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.CNPJ = reader.GetString("ds_CNPJ");
                dto.Email = reader.GetString("ds_email");
                dto.Endereco = reader.GetString("ds_endereco");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Nr_Residencial = reader.GetString("nr_residencial");
                dto.CEP = reader.GetString("ds_cep");
                dto.Categoria = reader.GetString("ds_categoria");

                listaDTO.Add(dto);
            }

            reader.Close();
            return listaDTO;
           
        }

        public List<FornecedorDTO> Consultar (string nome)
        {
            string script = @"SELECT * FROM Tb_Fornecedores
                              WHERE nm_fornecedor like @nm_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", "%" + nome + "%"));

            List<FornecedorDTO> listaDTO = new List<FornecedorDTO>();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            while(reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.ID = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_fornecedor");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.CNPJ = reader.GetString("ds_CNPJ");
                dto.Email = reader.GetString("ds_email");
                dto.Endereco = reader.GetString("ds_endereco");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Nr_Residencial = reader.GetString("nr_residencial");
                dto.CEP = reader.GetString("ds_cep");
                dto.Categoria = reader.GetString("ds_categoria");

                listaDTO.Add(dto);
            }

            reader.Close();
            return listaDTO;
            
        }
    }
}
