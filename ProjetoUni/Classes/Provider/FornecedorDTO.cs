﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoUni.Classes.Fornecedor
{
 public class FornecedorDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string CNPJ { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Nr_Residencial { get; set; }
        public string CEP { get; set; }
        public string Categoria { get; set; }
    }
}
