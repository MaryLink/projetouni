﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoUni.Telas;

namespace ProjetoUni
{
    public partial class TelaSplash : Form
    {
        public TelaSplash()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
               {
                   FrmInicial frm = new FrmInicial();
                   frm.Show();
                   Hide();
               }));
            });
        }
    }
}
